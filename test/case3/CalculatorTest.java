package case3;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CalculatorTest {
	
    @Test
    public void testSum() {
        Calculator calculator = new Calculator();        
        long sum = calculator.sum(1, 100, new Sum());        
        assertEquals(5050, sum);
    }
	
    @Test
    public void testSumOfSquares() {
        Calculator calculator = new Calculator();
        long sum = calculator.sum(1, 10, new SumOfSquares());
        assertEquals(385, sum);
    }
	
}
