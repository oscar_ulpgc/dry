
package case3;

public interface Function {
    
    public double apply(double value);
    
}
