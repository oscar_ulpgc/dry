package case3;

public class Calculator {

    public long sum(int min, int max, Function f) {
        long result = 0;
        for (int i = min ; i <= max ; i++)
            result += f.apply(i);
        return result;
    }

}
