
package case3;

import case3.Function;

class SumOfSquares implements Function {

    @Override
    public double apply(double value) {
        return value * value;
    }
    
}
